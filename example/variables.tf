variable "OrgID" {
  description = "Org ID required for the access of all trusted accounts in the org"
  type        = string
  default = "<<YOUR_ORG_ID>>"
}

variable "region" {
  description = "region"
  type        = string
  default     = "eu-west-2"
}