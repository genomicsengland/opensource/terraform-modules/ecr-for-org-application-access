module "ecr_creation" {
  source                  = "../"
  name                    = "my_name2"
  OrgID                   = var.OrgID
  push_pull_trusted_roles = ["ROLE1", "ROLE2"]
  #mutable_tags    = true
}

# Create an repository and pass extra tags or tags that override those in the provider (used to set correct owner tags on repositories in core_bakery but owned by product squad)
module "ecr_creation_with_extra_tags" {
  source                  = "../"
  name                    = "shared_squad_repo"
  OrgID                   = var.OrgID
  push_pull_trusted_roles = ["ROLE1", "ROLE2"]

  override_repo_tags = {
    Squad    = "a product squad"
    ExtraTag = "an additional tag added to those from provider"
  }
}