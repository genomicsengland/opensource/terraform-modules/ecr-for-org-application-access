output "arn" {
  value = aws_ecr_repository.ecr_repository.arn
  description = "ECR Repository ARN"
}

output "registryId" {
  value = aws_ecr_repository.ecr_repository.registry_id
  description = "ECR Repository Registry Id"
}

output "url" {
  value = aws_ecr_repository.ecr_repository.repository_url
  description = "ECR Repository URL"
}