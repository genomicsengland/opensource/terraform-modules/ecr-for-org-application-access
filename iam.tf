data "aws_iam_policy_document" "push_pull_iam_policy_document" {
  statement {
    actions = [
      "ecr:GetDownloadUrlForLayer",
      "ecr:DescribeImages",
      "ecr:BatchGetImage",
      "ecr:ListImages",
    ]
    principals {
      type        = "AWS"
      identifiers = ["*"]
    }
    condition {
      test     = "StringEquals"
      variable = "aws:PrincipalOrgID"
      values   = [var.OrgID]
    }
  }

  statement {
    sid = "LambdaECRImageRetrievalPolicy"

    actions = [
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchGetImage",
      "ecr:ListImages",
    ]
    principals {
      type        = "Service"
      identifiers = ["lambda.amazonaws.com"]
    }
    condition {
      test     = "StringEquals"
      variable = "aws:PrincipalOrgID"
      values   = [var.OrgID]
    }
  }

  statement {
    actions = [
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchGetImage",
      "ecr:BatchCheckLayerAvailability",
      "ecr:DescribeRepositories",
      "ecr:GetLifecyclePolicy",
      "ecr:ListImages",
      "ecr:CompleteLayerUpload",
      "ecr:InitiateLayerUpload",
      "ecr:PutImage",
      "ecr:UploadLayerPart"
    ]

    principals {
      type        = "AWS"
      identifiers = var.push_pull_trusted_roles
    }
  }
}

data "aws_iam_policy_document" "delete_iam_policy_document" {
  count = var.delete_image_trusted_roles != null ? 1 : 0

  statement {
    sid = "AllowDeleteImage"

    actions = ["ecr:BatchDeleteImage"]

    principals {
      type        = "AWS"
      identifiers = var.delete_image_trusted_roles
    }
  }
}

data "aws_iam_policy_document" "combined_iam_policy_document" {

  source_policy_documents = (
    var.delete_image_trusted_roles == null ? [data.aws_iam_policy_document.push_pull_iam_policy_document.json]
    :
    [
      data.aws_iam_policy_document.push_pull_iam_policy_document.json,
      data.aws_iam_policy_document.delete_iam_policy_document[0].json
    ]
  )
}