variable "name" {
  description = "Name"
  type        = string
}

variable "OrgID" {
  description = "KMS CMK Bakery ARN - set to your environments CMK outside bakery"
  type        = string
}

variable "push_pull_trusted_roles" {
  description = "List of roles to trust to create ECR"
  type        = list
}

variable "delete_image_trusted_roles" {
  description = "List of roles allowed to delete images from ECR"
  type        = list
  default     = null
}

variable "mutable_tags" {
  description = "Enable/Disable Mutable tagging"
  type = bool
  default = false
}

variable "override_repo_tags" {
  description = "A set of tags to attach to the repo created - allows specific tags to be passed that are not same as those of the calling provider"
  default     = {}
}